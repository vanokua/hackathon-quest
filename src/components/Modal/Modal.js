import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import './Modal.css';


class MyModal extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            show: false
        };

    }

    handleHide = () => {
        this.setState({ show: false });
    };

    render() {
        return (
            <div className="modal-container" style={{ height: 200 }}>
                {/*<Button*/}
                    {/*bsStyle="primary"*/}
                    {/*bsSize="large"*/}
                    {/*onClick={() => this.setState({ show: true })}*/}
                {/*>*/}
                    {/*Launch contained modal*/}
                {/*</Button>*/}

                <Modal
                    show={true}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header>
                        <Modal.Title id="contained-modal-title" className='text-center'>
                            You Did It!
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="pyro">
                            <div className="before"></div>
                            <div className="after"></div>
                        </div>
                        <h1 className='text-center'>CONGRATSS!!!!!!!</h1>
                    </Modal.Body>
                    <Modal.Footer className='text-center'>
                        <Button bsStyle="success" onClick={this.handleHide}>Next Task</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default MyModal;