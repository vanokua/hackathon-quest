import React  from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Button
} from 'react-bootstrap';

class Login extends React.Component {
  state = {
    login: '',
    pass: ''
  }

  handleChangeLogin = (e) => {
    this.setState({ login: e.target.value });
  }

  handleChangePass = (e) => {
    this.setState({ pass: e.target.value });
  }

  submit = (event) => {
    event.preventDefault();
    this.props.login(this.state.login, this.state.pass);
  }

  render() {
    return (
      <form onSubmit={this.submit}>
        <FormGroup>
          <ControlLabel>Login</ControlLabel>
          <FormControl
            type="text"
            value={this.state.login}
            onChange={this.handleChangeLogin}
            required
          />
        </FormGroup>

        <FormGroup>
          <ControlLabel>Pass</ControlLabel>
          <FormControl
            type="password"
            value={this.state.pass}
            onChange={this.handleChangePass}
            required
          />
        </FormGroup>

        <Button
          bsStyle="primary"
          type="submit"
          onClick={this.login}
          disabled={!this.state.login || !this.state.pass}
        >
            Submit
        </Button>
      </form>
    )
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired
};

export default Login;
