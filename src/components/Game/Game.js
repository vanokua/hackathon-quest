import React from 'react'
import './Game.css'
import { connect } from 'react-redux'
import { gameCompleted } from '../../reducers/game'

const mapDispatchToProps = {
    gameCompleted,
};

class Game extends React.Component{
    componentDidMount() {

        window.requestAnimationFrame(() => {
            const manager = new window.GameManager(4, window.KeyboardInputManager, window.HTMLActuator);
            window.updateScore(() => {
                if (manager.score > 100) {
                    // this.props.gameCompleted(true);
                    this.props.handleSubmit();
                }
            });
        });

    }

    render() {
        return (
            <div className="container">
                <div className="heading">
                    <h1 className="title">2048</h1>
                    <div className="score-container">0</div>
                </div>

                <div className="game-container">
                    <div className="game-message">
                        <p></p>
                        <div className="lower">
                            <a className="retry-button">Retry</a>
                        </div>
                    </div>

                    <div className="grid-container">
                        <div className="grid-row">
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                        </div>
                        <div className="grid-row">
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                        </div>
                        <div className="grid-row">
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                        </div>
                        <div className="grid-row">
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                            <div className="grid-cell"></div>
                        </div>
                    </div>
                    <div className="tile-container">
                    </div>
                </div>
            </div>
    )
    }
}

export default connect(null, mapDispatchToProps)(Game);