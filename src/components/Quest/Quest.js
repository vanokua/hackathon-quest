import React from 'react'
import './Quest.css'
import { FormGroup, FormControl, HelpBlock, Button } from 'react-bootstrap';

class Quest extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);

        this.state = {
            value: ''
        };
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    render() {
        const { handleSubmit, quest } = this.props;
        return (
            <div className='quest-container'>
                <h1>{quest.name}</h1>
                <h3 dangerouslySetInnerHTML={{__html: quest.taskText}}></h3>
                {quest.initialTask &&
                    <form>
                        <FormGroup
                            controlId="formBasicText"
                        >
                            <FormControl
                                type="text"
                                placeholder="Enter keyword here"
                                value={this.state.value}
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <HelpBlock>Here will be some helpfull text</HelpBlock>
                        </FormGroup>
                        <Button onClick={() => handleSubmit(this.state.value)}>Submit Keyword!</Button>
                    </form>

                }

            </div>
        );
    }
}

export default Quest
