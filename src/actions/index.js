const uri = 'ws://localhost:8080';

/**
 * WebSocket
 */

const socket = new WebSocket(uri);

export const websocketInit = ( dispatch ) => {
  socket.onopen = (event) => {
    // socket.send('Hello Server!');
  };
  socket.onmessage = (event) => {
    console.log('Message from server ', event);
    console.log('Data ', event.data);

    // dispatch({type, payload: event.data});
  };
  socket.onerror = (error) => {
    console.log('WebSocket Error: ', error);
  };
  socket.onclose = (event) => {
    console.log('Disconnected from WebSocket.');
  };
}

export const emit = (type, payload) => socket.send(JSON.stringify({type, payload}));


/**
 * Auth
 */

const LOGIN = 'LOGIN';
const loginRequest = (name, pass) => ({
  type: LOGIN,
  name,
  pass
})

const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const loginSuccess = (name, pass) => ({
  type: LOGIN_SUCCESS
})

const LOGIN_FAILED = 'LOGIN_FAILED';
const loginFailed = (name, pass) => ({
  type: LOGIN_FAILED
})

export const login = (name, pass) => (dispatch) => {
  console.log(name, pass);
  dispatch(loginRequest(name, pass));
  emit(LOGIN, {name, pass})
}
