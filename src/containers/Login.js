import React from 'react'
import { connect } from 'react-redux'
import Login from '../components/Login'
import { login } from '../actions'

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => ({
  login: (name, pass) => dispatch(login(name, pass))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
