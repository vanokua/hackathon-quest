import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Game from '../components/Game/Game'
import Quest from '../components/Quest/Quest'
import Loader from '../components/Loader/Loader'
import Modal from '../components/Modal/Modal'
import { updateTask, questCompleted } from '../reducers/quest'

import Login from './Login'

const quests = [
    {
        name: 'game',
        taskText: null,
        initialTask: true
    },
    {
        name: 'book',
        taskText: 'this will be some sub task from book',
        initialTask: true
    },
    {
        name: 'book',
        taskText: 'this will be some sub task from book',
        initialTask: false
    }
];

class App extends React.Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  }

    componentDidMount() {
        // here we should get request named as getQuest or something
        setTimeout(() => {
            this.updateTask(quests[0]);
        }, 3000);
    }

    updateTask(task) {
        this.props.updateTask(task);
    }

    handleSubmit = (value) => {
        // here will be request to emit value of quest and get new sub task
        // if main task is a game need just 1 request
        setTimeout(() => {
            // this.updateTask(quests[2]);
            this.props.questCompleted();
        }, 2000);
    };

    getElement(name) {
        switch (name) {
            case 'game': {
                return <Game handleSubmit={this.handleSubmit}/>;
            }
            default: {
                return <Quest quest={this.props.quest} handleSubmit={this.handleSubmit}/>;
            }
        }
    }

  render () {
    return (
      <div className='app-container'>
        {
          this.props.auth && this.props.auth.token ?
              <div>
                  {
                      this.props.quest.isLoading ?  <Loader/> : this.getElement(this.props.quest.name)
                  }
                  {
                      this.props.quest.completed && <Modal/>
                  }
              </div> :
            <Login />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    quest: state.quest,
  }
}

const mapDispatchToProps = {
    updateTask,
    questCompleted
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
