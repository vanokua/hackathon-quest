import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import configureStore from "./store/configureStore";
import { websocketInit } from "./actions";

const store = configureStore();
websocketInit(store.dispatch);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
