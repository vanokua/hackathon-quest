const QUEST_UPDATE = 'QUEST_UPDATE';
const QUEST_COMPLETE = 'QUEST_COMPLETE';

export const updateTask = (quest) => ({
    type:  QUEST_UPDATE,
    payload: quest
});

export const questCompleted = () => ({
    type:  QUEST_COMPLETE,
});

const initialState = {
    name: null,
    taskText: null,
    initialTask: false,
    completed: false,
    isLoading: true
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case QUEST_UPDATE: {
            return { ...state, ...action.payload, isLoading: false  };
        }
        case QUEST_COMPLETE: {
            return { ...state, completed: true };
        }
        default: {
            return state;
        }
    }
};

