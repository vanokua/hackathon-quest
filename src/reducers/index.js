import { combineReducers } from 'redux'
import game from './game'
import quest from './quest'

const auth = (state = {}, action) => state;

const rootReducer = combineReducers({
    auth,
    game,
    quest,
});

export default rootReducer
