const GAME_SCORE_UPDATE = 'GAME_SCORE_UPDATE';

const initialState = {
    completed: false
};

export const gameCompleted = (completed) => ({
    type:  GAME_SCORE_UPDATE,
    completed
});

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case GAME_SCORE_UPDATE: {
            const { completed } = action;
            return { ...state, completed};
        }
        default: {
            return state;
        }
    }
};

